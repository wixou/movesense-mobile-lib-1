## (Oct 23rd 2018) Update to mobile libraries  

### Android and iOS (MDS 1.28.1, Whiteboard 3.16.3, libsbem 1.7)

###Whats new:
- Bugfix(Android): BleManager - MissingBackpressureException (#75)
- Bugfix(Android): Match MAC on disconnect / Reconnection fails when multiple sensors are used (#74)
- Bugfix(Android): Propagate RxBle errors to onConnectError (#75)
- Technical: Whiteboard updated to v 3.16.3


## (Oct 3rd 2018) Update to mobile libraries

Movesense mobile libraries (MDS) for both iOS and Android have been updated. See below for list of changes per platform.

### Android (MDS 1.25.0, Whiteboard 3.16.0, libsbem 1.7) ###  

  - Technical: Logbook json conversion bugfixes and improvements
  - Technical: ResourceId caching for non pathmeter resources (less traffic to get and release remote resource ids)
  - Technical: Don't log email addresses (GDPR Compliancy)
  - Technical: Light in-memory cache Logbook/Data resource
  - Technical(iOS): Enable use of an external CBCentralManager to be provided to WBCentral
  - Technical(iOS): Added device discovery control to iOS MDSWrapper API
  - Technical(iOS): Forward debug logging to iOS system log
  - Technical(iOS): External device UUID list in init() for connecting only interested devices
  - Technical(iOS): Add public MDS log level API for iOS (sets logging level for suuntoapp.log output)
  - Bugfix: Descriptor cleanup crash when device has been updated
  - Bugfix: Missing DEL event in disconnect
  - Bugfix: MDS shutdown crash in ResourceClient destruction
  - Bugfix: Protect callback thread joining in ResourceClient
  - Bugfix: Add missing UUID-field to GET ConnectedDevices response
  - Bugfix: Manually POSTing serial devices
  - Bugfix: Fix only partially downloaded Logbook entries
  - Bugfix(Android): Concurrent modification exception
  - Bugfix(iOS): Added logic for re-pairing a device even it has not been removed from iOS BT paired list.
  - Bugfix(iOS): Modified reconnection flow to fix the sensor connectivity issues
  - Bugfix(iOS): iOS BLE-plugin parallel connections
  - Bugfix(iOS): WBCentral restorestate handling fixies
  - Bugfix(iOS): Pass BLE received data forward immediately without dispatch queue
  - Bugfix(iOS): Add more checks to prevent crash if the response contains invalid utf-8 characters 

### Android (MDS 1.13.0, Whiteboard 3.12.0, libsbem 1.3) ###  

Changes compared to previous library version (1.10.0) on Android:

  - Technical: Reduce default WB retries from 20 to 3
  - Technical: SDS version resource for whiteboard devices
  - Technical: DeviceConnector to support WB devices without InetGW
  - Technical: Allow NoType to be used in Bypass subscription
  - Technical: Serialize BusyState subscription
  - Technical: Disable BusyState subscription by default
  - Technical: Added response code to MdsResponseListener
  - Technical: Device busy subscription to ResourceClient
  - Bugfix: Connect without serial in device discovery
  - Bugfix: Add public MDS log level API for Android
  - Bugfix: Synchronize mIBleConnectionMonitorArrayList usage in BleManager
  - Bugfix: Fix IllegalStateException where RxBleConnection subscription and internal devicesMap are not in sync
  - Bugfix: Prevent NPE when callback is null
  - Bugfix: Fix simultaneous connection issue with builds having busy subscription enabled
  - Bugfix: Send unsubscriptions to providers in application when subscribed device disconnects

### iOS (MDS 1.13.0, Whiteboard 3.12.0, libsbem 1.3) ###

Changes compared to previous library version (1.7.0) on iOS:

  - Technical: Reduce default WB retries from 20 to 3
  - Technical: SDS version resource for whiteboard devices
  - Technical: DeviceConnector to support WB devices without InetGW
  - Technical: Allow NoType to be used in Bypass subscription
  - Technical: Serialize BusyState subscription
  - Technical: Disable BusyState subscription by default
  - Technical: Device busy subscription to ResourceClient
  - Technical: iOS MDSWrapper to set resource request method as SUBSCRIBE/UNSUBSCRIBE instead of POST/DEL
  - Technical: iOS MDSWrapper improvements
  - WB resource registeration
  - Support ByteStream type without base64 encoding
  - Technical: Release metadata hash resource if retrieved during WB operation
  - Bugfix: Connect without serial in device discovery
  - Bugfix: Fix simultaneous connection issue with builds having busy subscription enabled
  - Bugfix: Send unsubscriptions to providers in application when subscribed device disconnects
  - Bugfix: Fix possible unsubscription race condition crash at device disconnect
  - Bugfix: Fix issue where disconnect during handshake causes device to appear connected

---

# Release notes for earlier releases #

## More samples release ##

### Whats new: ###
   **NOTE**: This is an Android samples only release, library version and Showcase app (sampleapp) stay the same)
   - Improved ConnectivityAPISample
   - Added SensorSample
   - Added ECGSample


## Android Library Version: 1.6.1 ##
## iOS Library Version: 1.3.1 ##
## Application Version: 1.3 ##

### Whats new: ###
   **NOTE**: This is an Android only release (i.e. the changes are only about the Android version)
   - Improve DFU update process ( fix issue with device disconnected error)
   - Add apk with Android sample App in directory android/samples/sampleapp/
   - Fix 404 error after changing device name
   - Add support for dfu manufacture data ( fix issue with unknown macAddress when Movesense has manufacture data writen)
   - Fix typo in Magnetic Field
   - Fix null serial and sw version after device name change
   - Add information about connected device into main screen after connection
   - Fix dfu incrementation with
   - Fix connection issue with fast stop scanning click before connection was established
   
## Android Library Version: 1.6.0 ##
## iOS Library Version: 1.3.1 ##
## Application Version: 1.2 ##

### Whats new: ###
   **NOTE**: This is an Android only release (i.e. the changes are only about the Android version)
   - Moved the Android samples under separate folder
   - The .aar package can now be found in folder *./Android/Movesense/*
   - New simple *Connectivity API* for Android MDS library
   - New sample for Android: "ConnectivityAPISample"
   - NOTE: The *sampleapp* still uses the old connectivity

   (No known bugs since half of the samples use still the old method)

## Library Version: 1.3.1 ##
## Application Version: 1.2 ##

### Whats new: ###

   - Improvment UI design
   - DFU is moved to the main screen
   - Added MultiConnection
   - Added Version Library and Application on the main view
   - Toolbars changing name to current view

### Known Bugs: ###

   - Movesense not reconnecting with app after bluetooth off / on.
   - Nexus 6 can't connect with Movesense ( Android platform BUG )

## Library Version: 1.3.1 ##
## Application Version: 1.1 ##

### Whats new: ###

   - Fixed: Memory leaks for longer sensor subscriptions ( BackPressureException ) 
   - Recovery for non-intentionall disconnect ( Movesense out-of-range / disconnect / connection lost )
   - Validation for DFU file ( sometimes file was corrupted by Google Drive )
   - Subscription is not stopped when devices is going to sleep.
   - Allow connection for new and old software ( changes in JSON response)
   - More Unit Tests for library.
   
### Known Bugs: ###

   - When DFU update will fail at some point then may have problem with connection because Movesense is still in DFU mode ( solution -> DFU will be moved to main view in next Application version )
   - Movesense sometimes can't reconnect to Application.
   - Movesense not reconnecting with app after bluetooth off / on.
